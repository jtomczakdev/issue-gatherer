# README #

### What is this repository for? ###

Python script to pull Jira cloud issues and associated data when issue count is over 1k issues.

### How do I get set up? ###

Jira Configuration:

* Create a new filter or use an existing filter

* Select the List view option on the filter

* In the Column dropdown, choose the filter tab and select the fields you want included in the CSV

Script Configuration:

* Download Files

* Run the file using Python 3

* Supply the required arguments (URL,FILTER, EMAIL, TOKEN, NAME)

* File will be generated in the same location as the script with the supplied name.


### Troubleshooting ###

#### Columns are shifted and displaying the wrong data in each column? ####

The script is encountering a field type that does not know how to be formatted. You will need to do the following:

* You need to identify the type of field by using this endpoint: xxxxx.atlassian.net/rest/api/3/field
* Under schema > custom you will find the type, will look similar to "com.atlassian.jira.plugin.system.customfieldtypes:people" as an example. 
* find an issue that has that field populated and use the following endpoint: xxx.atlassian.net/rest/api/2/issue/{issuekey} where "{issuekey}" is the key of the issue.
* find the customfield data and see how the data is stored, sometimes it is stored with a "value" key, sometimes it is stored under "customfield_xxxxx", etc
* in the script in the CUSTOM FIELDS section, you will need to add the custom field type to the array that it matches, use the comments as a guide for assistance. 